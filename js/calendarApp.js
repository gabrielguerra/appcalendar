let cal = null;
let func = null;

class Cells{
    constructor(calInstance){
        this.calInstance = calInstance;
    }

    //Renders cells from prior Month
    renderPriorMonthCells(){
        let output = "";
        let cellId = null;
        let calInstance = this.calInstance;

        let numShiftCells = calInstance.getNumDaysShift(calInstance.refDate);

        //Prior month
        let priorMonth = calInstance.getPriorMonth();
        let numOfDaysPriorMonth = calInstance.numOfDays[priorMonth];
        let priorMonthDay = (numOfDaysPriorMonth - numShiftCells)+1;

        for (let i=1; i < parseInt(numShiftCells,10)+1; i++){
            //Fill with the cells from prior month
            let priorYear = calInstance.activeYear;
            if (parseInt(calInstance.getIdMonth(),10)===12){
                priorYear = parseInt(priorYear, 10)-1;
            }
            cellId =  priorYear + "-" + calInstance.getIdMonth() + "-" + priorMonthDay;
            output += `
                <div class="calendar-cell prior-month" id="${cellId}">
                    ${priorMonthDay}
                    <div class="cell-icon">
                    </div>
                </div>`;

            //Control vars
            numShiftCells -= 1;
            priorMonthDay++;
            i--;
        }
        return output;
    }

    //Renders cells from the selected Month
    renderActiveMonthCells(){
        let calInstance = this.calInstance;
        let output = "";
        let thisMonth = parseInt(calInstance.activeMonth,10)+1;
        for (let i=1; i<(calInstance.activeMonthNumDays+1); i++) {
            let cellId = calInstance.activeYear + "-" + thisMonth + "-" + i;

            output += `
                <div class="calendar-cell" id="${cellId}">
                    ${i}<div class="cell-icon"></div>
                </div>`;
        }
        return output;
    }

    //Completes the calendar with subsequent cells if the Month it's not December
    renderFutureCells(){
        let calInstance = this.calInstance;
        let output = "";
        let thisMonth = parseInt(calInstance.activeMonth,10)+2;
        //How many cells must we render?
        let numUsedCells = (calInstance.activeMonthNumDays) + (calInstance.getNumDaysShift(calInstance.refDate));
        let numRows = Math.floor((numUsedCells/7)+1);
        let numExtraCells = (numRows*7)-numUsedCells;

        if (numUsedCells%7!==0 && thisMonth!==13) {
            for (let i = 1; i <= numExtraCells; i++) {
                let cellId = calInstance.activeYear + "-" + thisMonth + "-" + i;
                output += `
                <div class="calendar-cell prior-month" id="${cellId}">
                    ${i}<div class="cell-icon"></div>
                </div>`;
            }
        }
        return output;
    }
}

//A simple object structure for appointments handling
class Appointment{
    constructor(id, title, time){
        this.id = id;
        this.title = title;
        this.time = time;
    }
}

class Calendar{

    constructor(){
        this.activeYear = 2019;
        this.activeMonth = 0;
        this.activeMonthNumDays = null;
        this.numOfDays = Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        this.appointments = Array();
        this.dayOfEvent = null;

        //Reference used to shift cells in the month beginning
        this.refDate = this.activeYear + "/01/01";

        this.initWithDummyData();
        this.renderComboTime();
    }

    initWithDummyData(){
        /* Dummy data */
        this.addNewAppointmentObj("2019-1-10", "Study Spotify API", "12:20");
        this.addNewAppointmentObj("2019-1-15", "Test Spotify API", "13:30");
        this.addNewAppointmentObj("2019-1-16", "Take a Break", "09:15");
        /* *** */
        this.renderComboMonths();
        this.setSelectedMonth(0);
        this.renderCal();
        this.markCellsWithAppointments();
    }

    //Returns the Month index
    getMonthByIndex(index){
        const d = new Date(1900, index, 1);
        const m = d.toLocaleString("en-us", { month: "long" });
        return m;
    }

    //Returns how many cells we have to shift in the beginning of the Month
    getNumDaysShift(myDate){
        let newDate = new Date(myDate);
        return newDate.getDay();
    }

    //Gets the prior month relative to selected Month
    getPriorMonth(){
        //Get prior month info
        let priorMonth = parseInt(this.activeMonth,10)-1;
        //It's January?
        if (priorMonth === -1) {
            priorMonth = 11;
        }
        return priorMonth;
    }

    //Gets the right Month part in ID composition
    getIdMonth(){
        let idMonth = this.activeMonth;
        if (idMonth===0) {
            idMonth = 12;
        }
        return parseInt(idMonth, 10);
    }

    //Returns the objs position in Array
    getAppointmentById(id){
        let numAppointments = this.appointments.length;
        for (let i=0; i<numAppointments; i++) {
            let myId = this.appointments[i].id;
            if( myId === id) {
                //Returns obj position in Array
                return i;
            }
        }
        return false;
    }

    //Sets selected Month
    setSelectedMonth(monthSelected){
        this.activeMonth = monthSelected;
        this.activeMonthNumDays = this.numOfDays[monthSelected];
        this.refDate = this.activeYear + "/"+ ( parseInt(this.activeMonth, 10)+1 ) +"/01";
    }

    //Show active Appointments in //console
    logAppointments(){
        let numAppointments = this.appointments.length;
        for (let i=0; i<numAppointments; i++) {
            //console.log("App : %o", this.appointments[i]);
        }
    }

    //Marks all cells which have appointments
    markCellsWithAppointments(){
        let numAppointments = this.appointments.length;
        for (let i=0; i<numAppointments; i++) {
            let output = "";
            let target = "#"+this.appointments[i].id;
            let search = $("#cells").find(target);
            search.addClass("calendar-cell-app");
            let icon = "<i class=\"fa fa-check\"></i>";
            $(target+">.cell-icon").html(icon);
        }
    }

    //Removes an Appointment
    removeAppointment(id){
        let objPos = null;
        objPos = this.getAppointmentById(id);
        this.appointments.splice(objPos,1);
    }

    updateAppointment(id){
        let title = $("#eventTitle").val();
        let hours = $("#eventHours").val();
        let minutes = $("#eventMinutes").val();
        let time = hours+":"+minutes;

        if (cal.checkAgainstEmptyValues(title,  time)===false){
            return false;
        }
        cal.removeAppointment(cal.dayOfEvent);
        cal.addNewAppointmentObj(cal.dayOfEvent, title, time);
        cal.closeAppWindow();
    }

    handleAppointmentUpdate(id){
        //Sets the id/Day of reference
        this.dayOfEvent = id;
        /* *** */
        this.logAppointments();
        let objPos = this.getAppointmentById(id);
        let objsArray = this.appointments;
        let obj = objsArray[objPos];
        let title = obj.title;
        let time = obj.time.split(":");
        let minutes = time[1];

        this.showAppWindow("Edit Event", this.updateAppointment);

        $("#eventTitle").val(title);
        $("#eventHours").val(time[0]);
        $("#eventMinutes").val(minutes);
    }

    handleAppointments(id){
        //Sets the id/Day of reference
        this.dayOfEvent = id;
        /* *** */

        if (this.appointmentRules(id)) {
            this.showAppWindow("Add Appointment", this.makeAppointment);
        }
    }

    //Show the Appointment window
    showAppWindow(actionText, myFunc){
        $(".app-window").fadeIn();
        $(".app-window-title").html(actionText);
        this.renderComboTime();
        //Turns the passed function accessible to inner methods
        func = myFunc;
    }

    checkAgainstEmptyValues(title, time){
        if (title === ""){
            let message = "Give a name to your appointment, please.";
            cal.feedbackToUser(message);
            $("#eventTitle").focus();
            return false;
        }else{
            cal.hideFeedbackToUser();
        }

        if (time===":" || (time.split(":")[0]==="") || (time.split(":")[1]==="")) {
            let message = "When it's going to happen?";
            cal.feedbackToUser(message);
            $("#eventHours").focus();
            return false;
        }
    }

    //Prepares the appointment to be committed
    makeAppointment(){
        let title = $("#eventTitle").val();
        let hours = $("#eventHours").val();
        let minutes = $("#eventMinutes").val();
        let time = hours+":"+minutes;

        if (cal.checkAgainstEmptyValues(title,  time)===false){
            return false;
        }

        if(!cal.appointmentRules(cal.dayOfEvent)) {
            cal.closeAppWindow();
        }else{
            cal.addNewAppointmentObj(cal.dayOfEvent, title, time);
            cal.closeAppWindow();
        }
        cal.logAppointments();
    }

    //Add/commit a new Appointment
    addNewAppointmentObj(date, title, time){
        let myAppointment = new Appointment(date, title, time);
        this.appointments.push(myAppointment);
        this.renderAppointments();
    }

    //Simple criteria for Appointments commitment
    appointmentRules(id){
        if (this.getAppointmentById(id)!==false){
            alert("This date is already blocked.");
            return false;
        }else {
            let d1 = new Date();
            d1.setHours(0, 0, 0, 0);
            let d2 = new Date(id);
            d2.setHours(0, 0, 0, 0);

            if (d2.getTime() < d1.getTime()) {
                alert("Can't make an appointment in the past. Sorry :(");
                return false;
            }
        }
        return true;
    }

    //Closes the app window
    closeAppWindow(){
        $(".app-window").fadeOut();
        $("#eventTitle").val("");
    }

    feedbackToUser(message){
        let output = `<div class="alert alert-danger" role="alert">${message}</div>`;
        let target = $("#feedback-to-user");
        target.hide();
        target.html(output);
        target.fadeIn();
        //setTimeout(function(){ target.fadeOut(); }, 4000);
        return false;
    }

    hideFeedbackToUser(){
        $("#feedback-to-user").hide();
    }

    //Encapsulate the rendering process
    renderAll(){
        this.renderCal();
        this.renderAppointments();
        //this.logAppointments();
    }

    //Render the combo with Months
    renderComboMonths(){
        const numMonths = 12;
        let monthOptions = "<select class=\" form-control month-chooser\"><option>Choose a Month</option>";
        for (let i=0; i<numMonths; i++ ){
            monthOptions += `<option value="${i}">${this.getMonthByIndex(i)}</option>`;
        }
        monthOptions += "</select>";
        $("#monthSelector").html(monthOptions);
    }

    //Renders the Calendar
    renderCal(){
        let output = "";
        let cells = new Cells(this);

        //Fill with cells from prior month
        output += cells.renderPriorMonthCells();

        //Fill with cells from current month
        output += cells.renderActiveMonthCells();

        output += cells.renderFutureCells();

        //Clear float
        output += "<div class=\"clear\"></div>";

        //Insert the content
        $("#cells").html(output);
        $("#month").html(this.getMonthByIndex(this.activeMonth)+" "+this.activeYear);
    }

    //Renders the combo with time options
    renderComboTime(){
        this.renderComboHours();
        this.renderComboMinutes();
    }

    //Renders the hours combo
    renderComboHours(){
        let output = "<option value=\"\">Hour</option>";
        let hour = null;
        for (let i=0; i<=24; i+=1){
            if(i < 10){
                hour = "0"+i;
            }else{
                hour = i;
            }
            output += `<option value="${hour}">${hour}</option>`;
        }
        $("#eventHours").html(output);
    }

    //Renders the minutes combo
    renderComboMinutes(){
        let output = "<option value=\"\">Minutes</option>";
        let minutes = null;

        for (let i=0; i<60; i+=5){
            if(i < 10){
                minutes = "0"+i;
            }else{
                minutes = i;
            }
            output += `<option value="${minutes}">${minutes}</option>`;
        }
        $("#eventMinutes").html(output);
    }

    //@TODO Fix this comparison
    //Simple comparation for sorting the Array of Appointments objects
    compareDates(a,b) {

        let d1 = new Date(a.id.replace(/-/g, "/"));
        d1.setHours(0, 0, 0, 0);
        let d2 = new Date(b.id.replace(/-/g, "/"));
        d2.setHours(0, 0, 0, 0);

        if (d1.getTime() < d2.getTime()) {
            return -1;
        }

        if (d1.getTime() > d2.getTime()) {
            return 1;
        }
        return 0;
    }

    //Renders the Appointments column
    renderAppointments(){

        this.appointments.sort(this.compareDates);

        //Clear container
        $("#appointmentsContainer").html("");
        let numAppointments = this.appointments.length;
        let output = "";
        for (let i=0; i<numAppointments; i++){
            let obj = this.appointments[i];
            let date = obj.id.replace(/-/g, "/");
            let time = obj.time;

            output += `
            <div class="app-item">
                <div class="row">
                    <div class="col-md-10 col-sm-10">
                        <p class="gray"><i class="fa fa-calendar-alt gray"></i> ${date} <i class="fa fa-bell gray ml-20"></i> ${time}h</p>
                            <h5>${obj.title}</h5>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <i class="fa fa-edit edit-app" itemref="${obj.id}"></i> <i class="fa fa-trash-alt delete-app" itemref="${obj.id}"></i>
                    </div>
                </div>
            </div>`;
        }

        $("#appointmentsContainer").html(output);
        this.markCellsWithAppointments();
    }

}

$(document).ready(function(){

    //Make the App globally accessible
    cal = new Calendar();
    /* *** */

    // EVENTS
    $("#monthSelector").change(function(){
        let monthSelected = $("#monthSelector option:selected").val();
        cal.setSelectedMonth(parseInt(monthSelected,10));

        //Check for cells with appointments
        $.when( cal.renderCal() ).done(function() {
            cal.markCellsWithAppointments();
        });
        // cal.markCellsWithAppointments();
        cal.logAppointments();
    });

    $( document ).on( "click", ".edit-app", function() {
        let itemRef = $(this).attr("itemref");
        cal.handleAppointmentUpdate(itemRef);
        cal.hideFeedbackToUser();
    });

    $( document ).on( "click", ".delete-app", function() {
        let itemRef = $(this).attr("itemref");
        //let obj = cal.getAppointmentById(itemRef);
        cal.removeAppointment(itemRef);
        cal.renderAll();
    });

    $("#triggerEvent").click(function(){
            func();
    });

    $("#closeAppWindow").click(function(){
        cal.closeAppWindow();
    });

    $( document ).on( "click", ".calendar-cell", function() {
        let id = $(this).prop("id");
        let scare = "Buh!";
        cal.handleAppointments(id);
    });

});